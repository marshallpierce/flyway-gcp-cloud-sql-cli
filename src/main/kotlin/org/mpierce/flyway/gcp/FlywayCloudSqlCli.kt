package org.mpierce.flyway.gcp

import com.google.cloud.sql.postgres.SocketFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import joptsimple.OptionException
import joptsimple.OptionParser
import joptsimple.OptionSet
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.MigrationInfo
import org.slf4j.bridge.SLF4JBridgeHandler
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Properties
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()

    val parser = OptionParser()
    val configOpt = parser.accepts("c", "Properties file for db connection config")
        .withRequiredArg()
        .required()
    val locationOpt = parser.accepts("l", "Dir containing migrations")
        .withRequiredArg()
        .required()
    val modeOpt = parser.accepts(
        "m",
        "One of [${Mode.values().joinToString(", ")}]"
    )
        .withRequiredArg()
        .ofType(Mode::class.java)
        .required()
    val enableCleanOpt = parser.accepts("enable-clean", "Enable the 'clean' command")

    val opts: OptionSet
    try {
        opts = parser.parse(*args)
    } catch (e: OptionException) {
        System.err.println(e.message)
        parser.printHelpOn(System.err)
        exitProcess(1)
    }

    if (System.getenv("GOOGLE_APPLICATION_CREDENTIALS") == null) {
        System.err.println(
            "Env var GOOGLE_APPLICATION_CREDENTIALS must be set to the location of a credentials json file."
        )
        exitProcess(1)
    }

    val location = opts.valueOf(locationOpt)
    if (!Files.exists(Paths.get(location))) {
        System.err.println("Directory $location does not exist")
        exitProcess(1)
    }

    // TODO require confirmation if cleaning

    buildDataSource(Paths.get(opts.valueOf(configOpt))).use { ds ->
        val flyway = Flyway.configure().run {
            dataSource(ds)
            cleanDisabled(!opts.has(enableCleanOpt))
            locations("filesystem:$location")
            load()
        }

        when (opts.valueOf(modeOpt)!!) {
            Mode.info -> showInfo(flyway)
            Mode.validate -> {
                flyway.validate()
                println("Validation succeeded")
            }
            Mode.migrate -> {
                flyway.migrate()
            }
            Mode.clean -> {
                flyway.clean()
            }
        }
    }
}

fun buildDataSource(configPath: Path): HikariDataSource {
    val props = Files.newBufferedReader(configPath, StandardCharsets.UTF_8).use {
        val props = Properties()
        props.load(it)
        props
    }

    val dsConfig = HikariConfig().apply {
        jdbcUrl = "jdbc:postgresql://google/${props.getOrThrow("DB_NAME")}"
        username = props.getOrThrow("DB_USER")
        password = props.getOrThrow("DB_PASSWORD")
        isAutoCommit = false
        maximumPoolSize = 2 // don't bother opening too many connections -- maybe flyway only uses one?
    }
    dsConfig.addDataSourceProperty("socketFactory", SocketFactory::class.java.canonicalName)
    dsConfig.addDataSourceProperty("cloudSqlInstance", props.getOrThrow("CLOUD_SQL_INSTANCE_CONNECTION_NAME"))

    return HikariDataSource(dsConfig)
}

private fun Properties.getOrThrow(name: String): String {
    return getProperty(name) ?: throw IllegalArgumentException("No value defined for property <$name>")
}

private fun showInfo(flyway: Flyway) {
    val info = flyway.info()

    println("Applied migrations (${info.applied().size}):")
    info.applied().forEachIndexed { index, i ->
        println("${index + 1}. ${migrationDescription(i)}")
    }

    println("Pending migrations (${info.pending().size}):")
    info.pending().forEachIndexed { index, i ->
        println("${index + 1}. ${migrationDescription(i)}")
    }
}

private fun migrationDescription(info: MigrationInfo?): String {
    if (info == null) {
        return "(none)"
    }

    val output = StringBuilder("v${info.version} (${info.state}) - ${info.description} - ${info.script}")

    if (info.installedOn != null) {
        output.append(" - Installed ${info.installedOn.toInstant()}")
    }
    return output.toString()
}

// lower case names for better-looking CLI
@Suppress("EnumEntryName")
enum class Mode {
    info,
    validate,
    migrate,
    clean
}
