This allows applying Flyway operations (migrate, validate, etc) to a Google Cloud SQL Postgres instance. In essence, it's a recreation of the [Flyway CLI](https://flywaydb.org/documentation/commandline/) tailored for use with the [Cloud SQL JDBC socket factory](https://github.com/GoogleCloudPlatform/cloud-sql-jdbc-socket-factory).

It's possible that you could use the existing Flyway CLI, but the Cloud SQL adapter has a deep dependency tree that would be quite a pain to maintain in your local flyway CLI install.

# Configuration

Prepare a properties file like so:

```properties
CLOUD_SQL_INSTANCE_CONNECTION_NAME = ...
DB_NAME = ...
DB_USER = ...
DB_PASSWORD = ...
```

You'll also need a Google credentials json file. You can get one by creating a Service Account with suitable privileges in the Project containing the Cloud SQL instance you want to interact with.

# Usage

First, build the tool. Since gradle doesn't make it easy to pass CLI args and env vars to forked jvms, let gradle generate a standalone runnable distribution in `build/install`:

```
./gradlew installDist
```

Now, you could execute it (`./build/install/flyway-gcp-cloud-sql-cli/bin/flyway-gcp-cloud-sql-cli`),
but it won't work, of course, because you haven't configured anything yet...

This uses the Google Cloud Sql JDBC SocketFactory to allow accessing a Cloud Sql instance from outside GCP.
Unlike other GCP clients, the JDBC adapter only works with Application Default Credentials, which
requires setting the `GOOGLE_APPLICATION_CREDENTIALS` env var to point to a credentials json file.

Together with the necessary CLI args, an invocation would look something like this:

```
GOOGLE_APPLICATION_CREDENTIALS=path/to/credentials.json \
    ./build/install/flyway-gcp-cloud-sql-cli/bin/flyway-gcp-cloud-sql-cli \
    -c path-to-config-file.properties \
    -l path-to-parent-of-migrations-dir \
    -m mode
```

`mode` is one of `migrate`, `info`, etc.

Note that if using `clean`, you also have to pass `--enable-clean` as a safety measure.
