plugins {
    id("org.jetbrains.kotlin.jvm") version "1.5.20"
    id("application")
    id("com.github.ben-manes.versions") version "0.39.0"
    id("org.jmailen.kotlinter") version "3.4.5"
}

repositories {
    mavenCentral()
}

application {
    mainClass.set("org.mpierce.flyway.gcp.FlywayCloudSqlCliKt")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.flywaydb:flyway-core:7.10.0")
    implementation("net.sf.jopt-simple:jopt-simple:5.0.4")
    implementation("com.zaxxer:HikariCP:4.0.3")
    runtimeOnly("org.postgresql:postgresql:42.2.22")
    implementation("com.google.cloud.sql:postgres-socket-factory:1.3.0")

    implementation("org.slf4j:jul-to-slf4j:1.7.26")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.3")
}
